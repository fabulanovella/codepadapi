package com.wolfden.java.CodePadAPI;
/**
 * Created by Lachlan on 2/24/14.
 */
public class CompileResult {
	boolean HasErrors;
	private String[] Errors;
	private int ErrorCount;

	private String Output;

	public boolean isHasErrors() {
		return HasErrors;
	}

	public String[] getErrors() {
		return Errors;
	}

	public int getErrorCount() {
		return ErrorCount;
	}

	public String getOutput() {
		return Output;
	}

	public void setErrors(String[] errors) {
		ErrorCount = errors.length;
		Errors = errors;
		HasErrors = (ErrorCount > 0);
	}

	public void setOutput(String output) {
		Output = output;
	}
}
