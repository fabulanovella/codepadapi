package com.wolfden.java.CodePadAPI;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.*;
import java.net.*;
import java.util.ArrayList;

public class CodepadAPI {
	public static CodepadAPI Instance = new CodepadAPI();

	private URL cpUrl;

	public CodepadAPI() {
		try {
			cpUrl = new URL("http://codepad.org/");
		} catch (MalformedURLException e) {
			// Should be fine since the URL is static, but if you want to log
			// this or something go ahead
		}
	}

	public static CompileResult CompileCode(String code) {

		try {
			code = URLEncoder.encode(code, "utf-8");
			String params = "lang=C&private=True&run=True&submit=Submit&code="
					+ code;

			HttpURLConnection connection = (HttpURLConnection) Instance.cpUrl
					.openConnection();
			connection.setDoOutput(true);
			connection.setDoInput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type",
					"application/x-www-form-urlencoded");
			connection.setRequestProperty("charset", "utf-8"); // change the
																// encoding if
																// necessary. I
																// figure utf-8
																// is fine.
			connection.setRequestProperty("Content-Length",
					"" + Integer.toString(params.getBytes().length));
			connection.setUseCaches(false);

			OutputStreamWriter writer = new OutputStreamWriter(
					connection.getOutputStream());
			writer.write(params);
			writer.flush();

			String line;
			String response = "";
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream()));

			while ((line = reader.readLine()) != null) {
				response += line;
			}

			reader.close();
			writer.close();
			connection.disconnect();

			return ParseResponse(response);
		} catch (IOException e) {
			// Probably means that it couldn't connect, will cause it to return
			// null but you can make it display an error or something if you
			// want
		}
		return null;
	}

	private static CompileResult ParseResponse(String resp)
			throws UnsupportedEncodingException {
		CompileResult ret = new CompileResult();

		Document doc = Jsoup.parse(resp);

		Elements codeBoxes = doc.select(".code");
		Element outputBoxs = codeBoxes.get(1);

		Elements highlightBoxes = outputBoxs.select(".highlight");
		Element outputBox = highlightBoxes.get(1);
		String output = outputBox.text();

		ArrayList<String> errors = new ArrayList<String>();
		if (output.contains((": error: "))) {
			Elements errLinks = outputBox.select("a[href]");
			for (Element errLink : errLinks) {
				output = output.replace(errLink.text(), "\n" + errLink.text());
			}

			for (String err : output.split("\\n")) {
				if (!err.trim().equals("") && !err.trim().equals("\n"))
					errors.add(err);
			}

			ret.setOutput("");
		} else {
			output = output.replace("Exited: ", "\nExited: ");
			ret.setOutput(output);
		}

		String[] errArray = new String[errors.size()];
		ret.setErrors(errors.toArray(errArray));

		return ret;
	}

}
